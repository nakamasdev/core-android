package dev.nakamas.core.domain

import java.util.ArrayList

abstract class BaseMapper<I, O> {

    abstract fun map(input: I): O

    fun mapAll(inputs: Collection<I>): ArrayList<O> {
        val list = ArrayList<O>(inputs.size)
        for (input in inputs) {
            list.add(map(input))
        }
        return list
    }
}