package dev.nakamas.core.domain

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerProvider : BaseSchedulerProvider {
    override fun io() = Schedulers.io()
    override fun main() = AndroidSchedulers.mainThread()
}
