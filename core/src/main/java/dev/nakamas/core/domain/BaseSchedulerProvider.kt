package dev.nakamas.core.domain

import io.reactivex.Scheduler


interface BaseSchedulerProvider{
    fun io(): Scheduler
    fun main(): Scheduler
}

