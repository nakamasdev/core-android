package dev.nakamas.core.presentation.dialog

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog

class DialogManager {

    lateinit var dialog: MaterialDialog

    fun showProgressDialog(context: Context, title: String, message: String){
        dialog = MaterialDialog(context)
            .title(text = title)
            .message(text = message)
            .cancelable(false)
        dialog.show()
    }

    fun showDialog(
        context: Context,
        dialogModel: DialogModel
    ) {
        dialog = MaterialDialog(context)
            .title(text = dialogModel.title)
            .message(text = dialogModel.message)
            .cancelable(dialogModel.cancelable)

        when {
            validateDialogModel(dialogModel.positiveOptionModel) -> {
                dialog.positiveButton(text = dialogModel.positiveOptionModel?.value)
                dialog.positiveButton { dialogModel.positiveOptionModel?.callBack }
            }
            validateDialogModel(dialogModel.negativeOptionModel) -> {
                dialog.negativeButton(text = dialogModel.negativeOptionModel?.value)
                dialog.negativeButton { dialogModel.negativeOptionModel?.callBack }
            }
            else -> {
                dialog.positiveButton(text = "Aceptar")
                dialog.positiveButton {
                    it.dismiss()
                }
            }
        }

        dialog.show()
    }

    fun dismiss(){
        dialog.dismiss()
    }


    private fun validateDialogModel(dialogOptionModel: DialogOptionModel?): Boolean {
        return dialogOptionModel != null
    }


}

