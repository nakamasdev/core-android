package dev.nakamas.core.presentation.util

import android.annotation.SuppressLint

object RutValidator {

    const val MIN_RUT_LENGTH = 2
    const val MIN_RUT_VALID_LENGTH = 8

    fun format(value: String): String {
        var rut = value
        var cont = 0
        var format = ""
        if (rut.isNotEmpty()) {
            rut = rut.replace(".", "")
            rut = rut.replace("-", "")
            format = "-" + rut.substring(rut.length - 1)
            val rutLength = rut.length
            for (i in rutLength - 2 downTo 0) {
                format = rut.substring(i, i + 1) + format
                cont++
                if (cont == 3 && i != 0) {
                    format = ".$format"
                    cont = 0
                }
            }
        }
        return format
    }

    @SuppressLint("DefaultLocale")
    fun isValid(rutValue: String): Boolean {
        var rut = rutValue
        var validacion = false
        if (rut.length >= MIN_RUT_LENGTH) {
            try {
                rut = rut.toUpperCase()
                rut = rut.replace(".", "")
                rut = rut.replace("-", "")
                var rutAux = rut.substring(0, rut.length - 1).toInt()
                val dv = rut[rut.length - 1]
                var m = 0
                var s = 1
                while (rutAux != 0) {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11
                    rutAux /= 10
                }
                if (dv == (if (s != 0) s + 47 else 75).toChar()) {
                    validacion = true
                }
            } catch (e: NumberFormatException) {
            } catch (e: Exception) {
            }
        }
        return validacion
    }
}