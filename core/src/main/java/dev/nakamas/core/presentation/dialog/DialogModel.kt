package dev.nakamas.core.presentation.dialog

import com.afollestad.materialdialogs.DialogCallback


data class DialogModel(
    val title: String,
    val message: String,
    val cancelable: Boolean,
    val positiveOptionModel: DialogOptionModel?,
    val negativeOptionModel: DialogOptionModel?
)

data class DialogOptionModel(
    val value: String,
    val callBack: DialogCallback?
)