package dev.nakamas.core.presentation.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatEditText
import dev.nakamas.core.presentation.util.RutValidator


class RutEditText : AppCompatEditText {
    private var onFormatListener: OnFormatListener? = null

    constructor(context: Context?) : super(context) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView()
    }

    protected fun initView() {
        addTextChangedListener(RutFormatterTextWatcher())
        post { onFocusChangeListener = RutFormatterOnFocusChangeListener() }
    }

    fun setOnFormatListener(onFormatListener: OnFormatListener?) {
        this.onFormatListener = onFormatListener
    }

    private inner class RutFormatterTextWatcher : TextWatcher {
        private var lastRut = ""
        override fun beforeTextChanged(
            s: CharSequence,
            start: Int,
            count: Int,
            after: Int
        ) {
        }

        override fun onTextChanged(
            s: CharSequence,
            start: Int,
            before: Int,
            count: Int
        ) {
        }

        override fun afterTextChanged(s: Editable) {
            removeTextChangedListener(this)
            val rut = s.toString()
            if (lastRut != rut) {
                lastRut = rut
                val replacedRut = rut.replace(".", "").replace("-", "")
                if (replacedRut.length >= RutValidator.MIN_RUT_LENGTH &&
                    replacedRut.length <= RutValidator.MIN_RUT_VALID_LENGTH + 1
                ) {
                    val formattedRut: String = RutValidator.format(rut)
                    s.replace(0, s.length, formattedRut, 0, formattedRut.length)
                }
            }
            invalidate()
            addTextChangedListener(this)
        }
    }

    private inner class RutFormatterOnFocusChangeListener :
        OnFocusChangeListener {
        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            if (!hasFocus) {
                if (RutValidator.isValid(text.toString()) &&
                    text.toString().replace(".", "").replace(
                        "-",
                        ""
                    ).length >= RutValidator.MIN_RUT_VALID_LENGTH
                ) {
                    setText(RutValidator.format(text.toString()))
                    if (onFormatListener != null) {
                        onFormatListener!!.onFormatCorrect()
                    }
                } else {
                    if (text!!.length > 0) {
                        if (onFormatListener != null) {
                            onFormatListener!!.onFormatError()
                        }
                    }
                }
            }
        }
    }

    interface OnFormatListener {
        fun onFormatCorrect()
        fun onFormatError()
    }
}
