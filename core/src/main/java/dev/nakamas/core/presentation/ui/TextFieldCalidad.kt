package dev.nakamas.core.presentation.ui

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import androidx.annotation.Nullable
import androidx.appcompat.widget.AppCompatTextView
import dev.nakamas.core.R


class TextFieldCalidad : AppCompatTextView {
    private var tipoFuente = 0
    private var font: Typeface? = null

    constructor(context: Context) : super(context) {
    }

    constructor(context: Context, @Nullable attrs: AttributeSet) : super(
        context,
        attrs
    ) {
        init(attrs)
    }

    constructor(
        context: Context, @Nullable attrs: AttributeSet,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet) {
        val a =
            context.theme.obtainStyledAttributes(attrs, R.styleable.TextFieldCalidad, 0, 0)
        tipoFuente = try {
            a.getInteger(R.styleable.TextFieldCalidad_tipo_fuente, 0)
        } finally {
            a.recycle()
        }
        setFontCalidad()
    }

    private fun setFontCalidad() {
        try {
            if (tipoFuente == 0) {
                font = Typeface.createFromAsset(context.assets, "fonts/FjallaOne-Regular.ttf")
            } else if (tipoFuente == 1) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Black.ttf")
            } else if (tipoFuente == 2) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-BlackItalic.ttf")
            } else if (tipoFuente == 3) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Bold.ttf")
            } else if (tipoFuente == 4) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-BoldItalic.ttf")
            } else if (tipoFuente == 5) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Italic.ttf")
            } else if (tipoFuente == 6) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Light.ttf")
            } else if (tipoFuente == 7) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-LightItalic.ttf")
            } else if (tipoFuente == 8) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Medium.ttf")
            } else if (tipoFuente == 9) {
                font =
                    Typeface.createFromAsset(context.assets, "fonts/Roboto-MediumItalic.ttf")
            } else if (tipoFuente == 10) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Regular.ttf")
            } else if (tipoFuente == 11) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-Thin.ttf")
            } else if (tipoFuente == 12) {
                font = Typeface.createFromAsset(context.assets, "fonts/Roboto-ThinItalic.ttf")
            }
            this.typeface = font
        } catch (e: Exception) {
            Log.e("setFontCalidad", e.message)
        }
    }
}
