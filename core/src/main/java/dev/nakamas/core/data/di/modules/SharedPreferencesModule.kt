package dev.nakamas.core.data.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import dev.nakamas.core.R

@Module
object SharedPreferencesModule {

    @JvmStatic
    @Provides
    fun provideSharedPreferences(context: Context) =
        context.getSharedPreferences(
            context.getString(R.string.log_preferences_name),
            Context.MODE_PRIVATE
        )

}