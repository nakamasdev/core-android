package dev.nakamas.core.data.remote.api

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dev.nakamas.core.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    lateinit var retrofit: Retrofit
    val httpInterceptor: Interceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    var okHttpClient: OkHttpClient
    var baseUrl: String

    init {
        okHttpClient = OkHttpClient.Builder()
            .addInterceptor(httpInterceptor)
            .readTimeout(20, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()
        baseUrl = obtainURL()
    }

    private fun obtainURL(): String {
        return if (BuildConfig.DEBUG) {
            "https://api-ext-desa.ipchile.cl/api/"//"https://2c3d7d72-491f-4ffa-816e-85566e37ae3b.mock.pstmn.io/api/"
        } else {
            "https://api-ext-desa.ipchile.cl/api/"
        }
    }

    fun getClient(): Retrofit {
        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit
    }
}