package dev.nakamas.core.di

import dagger.Component
import javax.inject.Singleton

@Component
@Singleton
interface CoreComponent {
    //fun expensiveObject(): ExpensiveObject
}