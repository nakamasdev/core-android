package dev.nakamas.core.di

interface CoreComponentProvider {
    val component: CoreComponent
}