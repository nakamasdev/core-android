package dev.nakamas.core.di

import android.content.Context

object CoreInjectHelper {
    fun provideCoreComponent(applicationContext: Context): CoreComponent {
        return if (applicationContext is dev.nakamas.core.di.CoreComponentProvider) {
            (applicationContext as dev.nakamas.core.di.CoreComponentProvider).component
        } else {
            throw IllegalStateException("The application context you have passed does not implement CoreComponentProvider")
        }
    }
}