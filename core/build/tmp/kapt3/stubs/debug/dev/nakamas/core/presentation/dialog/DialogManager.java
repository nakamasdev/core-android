package dev.nakamas.core.presentation.dialog;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u001e\u0010\u0010\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012J\u0012\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0018"}, d2 = {"Ldev/nakamas/core/presentation/dialog/DialogManager;", "", "()V", "dialog", "Lcom/afollestad/materialdialogs/MaterialDialog;", "getDialog", "()Lcom/afollestad/materialdialogs/MaterialDialog;", "setDialog", "(Lcom/afollestad/materialdialogs/MaterialDialog;)V", "dismiss", "", "showDialog", "context", "Landroid/content/Context;", "dialogModel", "Ldev/nakamas/core/presentation/dialog/DialogModel;", "showProgressDialog", "title", "", "message", "validateDialogModel", "", "dialogOptionModel", "Ldev/nakamas/core/presentation/dialog/DialogOptionModel;", "core_debug"})
public final class DialogManager {
    @org.jetbrains.annotations.NotNull()
    public com.afollestad.materialdialogs.MaterialDialog dialog;
    
    @org.jetbrains.annotations.NotNull()
    public final com.afollestad.materialdialogs.MaterialDialog getDialog() {
        return null;
    }
    
    public final void setDialog(@org.jetbrains.annotations.NotNull()
    com.afollestad.materialdialogs.MaterialDialog p0) {
    }
    
    public final void showProgressDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    dev.nakamas.core.presentation.dialog.DialogModel dialogModel) {
    }
    
    public final void dismiss() {
    }
    
    private final boolean validateDialogModel(dev.nakamas.core.presentation.dialog.DialogOptionModel dialogOptionModel) {
        return false;
    }
    
    public DialogManager() {
        super();
    }
}