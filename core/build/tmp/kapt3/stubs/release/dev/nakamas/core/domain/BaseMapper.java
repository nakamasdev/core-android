package dev.nakamas.core.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\u0005\u001a\u00028\u00012\u0006\u0010\u0006\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0007J\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u000b\u00a8\u0006\f"}, d2 = {"Ldev/nakamas/core/domain/BaseMapper;", "I", "O", "", "()V", "map", "input", "(Ljava/lang/Object;)Ljava/lang/Object;", "mapAll", "Ljava/util/ArrayList;", "inputs", "", "core_release"})
public abstract class BaseMapper<I extends java.lang.Object, O extends java.lang.Object> {
    
    public abstract O map(I input);
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<O> mapAll(@org.jetbrains.annotations.NotNull()
    java.util.Collection<? extends I> inputs) {
        return null;
    }
    
    public BaseMapper() {
        super();
    }
}