package dev.nakamas.core.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004H\u0016\u00a8\u0006\u0007"}, d2 = {"Ldev/nakamas/core/domain/SchedulerProvider;", "Ldev/nakamas/core/domain/BaseSchedulerProvider;", "()V", "io", "Lio/reactivex/Scheduler;", "main", "kotlin.jvm.PlatformType", "core_release"})
public final class SchedulerProvider implements dev.nakamas.core.domain.BaseSchedulerProvider {
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Scheduler io() {
        return null;
    }
    
    @java.lang.Override()
    public io.reactivex.Scheduler main() {
        return null;
    }
    
    public SchedulerProvider() {
        super();
    }
}