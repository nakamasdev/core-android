package dev.nakamas.core.presentation.dialog;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\bH\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\bH\u00c6\u0003J?\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\bH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00062\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000e\u00a8\u0006\u001e"}, d2 = {"Ldev/nakamas/core/presentation/dialog/DialogModel;", "", "title", "", "message", "cancelable", "", "positiveOptionModel", "Ldev/nakamas/core/presentation/dialog/DialogOptionModel;", "negativeOptionModel", "(Ljava/lang/String;Ljava/lang/String;ZLdev/nakamas/core/presentation/dialog/DialogOptionModel;Ldev/nakamas/core/presentation/dialog/DialogOptionModel;)V", "getCancelable", "()Z", "getMessage", "()Ljava/lang/String;", "getNegativeOptionModel", "()Ldev/nakamas/core/presentation/dialog/DialogOptionModel;", "getPositiveOptionModel", "getTitle", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "core_release"})
public final class DialogModel {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String title = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String message = null;
    private final boolean cancelable = false;
    @org.jetbrains.annotations.Nullable()
    private final dev.nakamas.core.presentation.dialog.DialogOptionModel positiveOptionModel = null;
    @org.jetbrains.annotations.Nullable()
    private final dev.nakamas.core.presentation.dialog.DialogOptionModel negativeOptionModel = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTitle() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final boolean getCancelable() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final dev.nakamas.core.presentation.dialog.DialogOptionModel getPositiveOptionModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final dev.nakamas.core.presentation.dialog.DialogOptionModel getNegativeOptionModel() {
        return null;
    }
    
    public DialogModel(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean cancelable, @org.jetbrains.annotations.Nullable()
    dev.nakamas.core.presentation.dialog.DialogOptionModel positiveOptionModel, @org.jetbrains.annotations.Nullable()
    dev.nakamas.core.presentation.dialog.DialogOptionModel negativeOptionModel) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    public final boolean component3() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final dev.nakamas.core.presentation.dialog.DialogOptionModel component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final dev.nakamas.core.presentation.dialog.DialogOptionModel component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dev.nakamas.core.presentation.dialog.DialogModel copy(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean cancelable, @org.jetbrains.annotations.Nullable()
    dev.nakamas.core.presentation.dialog.DialogOptionModel positiveOptionModel, @org.jetbrains.annotations.Nullable()
    dev.nakamas.core.presentation.dialog.DialogOptionModel negativeOptionModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}