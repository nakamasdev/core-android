package dev.nakamas.core.presentation.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0007H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Ldev/nakamas/core/presentation/util/RutValidator;", "", "()V", "MIN_RUT_LENGTH", "", "MIN_RUT_VALID_LENGTH", "format", "", "value", "isValid", "", "rutValue", "core_release"})
public final class RutValidator {
    public static final int MIN_RUT_LENGTH = 2;
    public static final int MIN_RUT_VALID_LENGTH = 8;
    public static final dev.nakamas.core.presentation.util.RutValidator INSTANCE = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String format(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"DefaultLocale"})
    public final boolean isValid(@org.jetbrains.annotations.NotNull()
    java.lang.String rutValue) {
        return false;
    }
    
    private RutValidator() {
        super();
    }
}