package dev.nakamas.core.di;

import javax.annotation.Generated;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DaggerCoreComponent implements CoreComponent {
  private DaggerCoreComponent() {

  }

  public static Builder builder() {
    return new Builder();
  }

  public static CoreComponent create() {
    return new Builder().build();
  }

  public static final class Builder {
    private Builder() {
    }

    public CoreComponent build() {
      return new DaggerCoreComponent();
    }
  }
}
